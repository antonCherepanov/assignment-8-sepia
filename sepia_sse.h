#ifndef _SEPIA_SSE_H
#define _SEPIA_SSE_H
#include "bmp.h"
#include <stddef.h>
#include <stdlib.h>

void sepia_sse(struct image * in, struct image * out);

#endif //_SEPIA_SSE_H
