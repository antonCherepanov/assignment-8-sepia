#include "bmp.h"
#include "sepia_sse.h"

extern void _sepia_asm(struct pixel * input_data, size_t input_data_len, struct pixel * output_data);

void sepia_sse(struct image * in, struct image * out) {
	size_t pixel_size = in->width * in->height;
    size_t fin_size = (pixel_size / 4)*4;
    size_t rest = pixel_size - fin_size;
    out->width = in->width;
    out->height = in->height;
    out->data = malloc(sizeof(struct pixel) * out->width * out->height);
    _sepia_asm(in->data, in->height * in->width, out->data);
	for (size_t i = 0; i < rest; i ++) {
		out->data[i + fin_size] = * sepia_one(&in->data[i + fin_size]);
	}
}