#include <bits/types/FILE.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "bmp.h"
#include "write_bmp.h"

static void write_bit_map_file_header(FILE * out, const struct bit_map_file_header * bmfh);

static void write_bit_map_info_345(FILE * out, const struct bit_map_info_345 * bmi);

static void write_pixels(FILE * out, const struct image * img_to_write);

enum write_status to_bmp(FILE * out, const struct bmp_info * bmp) {
    write_bit_map_file_header(out, bmp->bmfh);
    write_bit_map_info_345(out, bmp->bmi);
    write_pixels(out, bmp->img);
    return WRITE_OK;
}

static void write_bit_map_file_header(FILE * out, const struct bit_map_file_header * bmfh) {
    fwrite(&bmfh->bf_type, sizeof(bmfh->bf_type), 1, out);
    fwrite(&bmfh->bf_size, sizeof(bmfh->bf_size), 1, out);
    fwrite(&bmfh->bf_reserved1, sizeof(bmfh->bf_reserved1), 1, out);
    fwrite(&bmfh->bf_reserved2, sizeof(bmfh->bf_reserved2), 1, out);
    fwrite(&bmfh->bf_off_bits, sizeof(bmfh->bf_off_bits), 1, out);
}

static void write_bit_map_info_345(FILE * out, const struct bit_map_info_345 * bmi) {
    fwrite(&bmi->bi_size, sizeof(bmi->bi_size), 1, out);
    fwrite(&bmi->bi_width, sizeof(bmi->bi_width), 1, out);
    fwrite(&bmi->bi_height, sizeof(bmi->bi_height), 1, out);
    fwrite(&bmi->bi_planes, sizeof(bmi->bi_planes), 1, out);
    fwrite(&bmi->bi_bit_count, sizeof(bmi->bi_bit_count), 1, out);
    fwrite(&bmi->bi_compression, sizeof(bmi->bi_compression), 1, out);
    fwrite(&bmi->bi_size_image, sizeof(bmi->bi_size_image), 1, out);
    fwrite(&bmi->bi_xpels_per_meter, sizeof(bmi->bi_xpels_per_meter), 1, out);
    fwrite(&bmi->bi_ypels_per_meter, sizeof(bmi->bi_ypels_per_meter), 1, out);
    fwrite(&bmi->bi_clr_used, sizeof(bmi->bi_clr_used), 1, out);
    fwrite(&bmi->bi_clr_important, sizeof(bmi->bi_clr_important), 1, out);
}

static void write_pixels(FILE * out, const struct image * img_to_write) {
    uint32_t line_offset = (4 - (img_to_write->width * 3 % 4)) % 4;
    uint8_t empty_arr[3] = {0};
    struct pixel * pixel_line = malloc(sizeof(struct pixel) * img_to_write->width);
    for (int i = 0; i < img_to_write->height; ++i) {
        uint32_t image_pixels_offset = (img_to_write->height - (i + 1)) * img_to_write->width;

        for (int j = 0; j < img_to_write->width; ++j) {
            pixel_line[j] = img_to_write->data[image_pixels_offset + j];
        }
        fwrite(pixel_line, sizeof(struct pixel), img_to_write->width, out);
        fwrite(empty_arr, sizeof(uint8_t), line_offset, out);
    }
}
