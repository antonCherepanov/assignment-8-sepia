#include <stdint.h>
#include <bits/types/FILE.h>
#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include "bmp.h"
#include "read_bmp.h"

static const uint16_t BF_TYPE1 = 0x4D42;
static const uint16_t BF_TYPE2 = 0x424D;

static struct bit_map_file_header * read_bit_map_file_header(FILE * in);

static bool is_bit_map_file_header_valid(const struct bit_map_file_header * bmfh);

static struct bit_map_info_345 * read_bit_map_info_345(FILE * in);

static void read_pixels(FILE * in, struct image * pix_buf);

enum read_status from_bmp(FILE * in, struct bmp_info * bmp) {
    bmp->bmfh = read_bit_map_file_header(in);
    if (!is_bit_map_file_header_valid(bmp->bmfh)) {
        return READ_INVALID_BIT_MAP_FILE_HEADER;
    }
    bmp->bmi = read_bit_map_info_345(in);
    if (bmp->bmi->bi_bit_count != 24) {
        return READ_INVALID_BIT_COUNT;
    }
    bmp->img = malloc(sizeof(struct image));
    bmp->img->width = bmp->bmi->bi_width;
    bmp->img->height = bmp->bmi->bi_height;
    fseek(in, bmp->bmfh->bf_off_bits, SEEK_SET);
    read_pixels(in, bmp->img);
    return READ_OK;
}

static struct bit_map_file_header * read_bit_map_file_header(FILE * in) {
    struct bit_map_file_header * bmfh = malloc(sizeof(struct bit_map_file_header));
    fread(&bmfh->bf_type, sizeof(bmfh->bf_type), 1, in);
    fread(&bmfh->bf_size, sizeof(bmfh->bf_size), 1, in);
    fread(&bmfh->bf_reserved1, sizeof(bmfh->bf_reserved1), 1, in);
    fread(&bmfh->bf_reserved2, sizeof(bmfh->bf_reserved2), 1, in);
    fread(&bmfh->bf_off_bits, sizeof(bmfh->bf_off_bits), 1, in);
    return bmfh;
}

static bool is_bit_map_file_header_valid(const struct bit_map_file_header * bmfh) {
    if (!(bmfh->bf_type == BF_TYPE1 || bmfh->bf_type == BF_TYPE2)) {
        return false;
    }
    if (bmfh->bf_reserved1 != 0 || bmfh->bf_reserved2 != 0) {
        return false;
    }
    return true;
}

static struct bit_map_info_345 * read_bit_map_info_345(FILE * in) {
    struct bit_map_info_345 * bmi = malloc(sizeof(struct bit_map_info_345));
    fread(&bmi->bi_size, sizeof(bmi->bi_size), 1, in);
    fread(&bmi->bi_width, sizeof(bmi->bi_width), 1, in);
    fread(&bmi->bi_height, sizeof(bmi->bi_height), 1, in);
    fread(&bmi->bi_planes, sizeof(bmi->bi_planes), 1, in);
    fread(&bmi->bi_bit_count, sizeof(bmi->bi_bit_count), 1, in);
    fread(&bmi->bi_compression, sizeof(bmi->bi_compression), 1, in);
    fread(&bmi->bi_size_image, sizeof(bmi->bi_size_image), 1, in);
    fread(&bmi->bi_xpels_per_meter, sizeof(bmi->bi_xpels_per_meter), 1, in);
    fread(&bmi->bi_ypels_per_meter, sizeof(bmi->bi_ypels_per_meter), 1, in);
    fread(&bmi->bi_clr_used, sizeof(bmi->bi_clr_used), 1, in);
    fread(&bmi->bi_clr_important, sizeof(bmi->bi_clr_important), 1, in);
    return bmi;
}

static void read_pixels(FILE * in, struct image * pix_buf) {
    uint32_t line_offset = (4 - (pix_buf->width * 3 % 4)) % 4;
    pix_buf->data = malloc(pix_buf->width * pix_buf->height * sizeof(struct pixel));
    struct pixel * pixel_line = malloc(sizeof(struct pixel) * pix_buf->width);

    for (int i = 0; i < pix_buf->height; ++i) {
        fread(pixel_line, sizeof(struct pixel), pix_buf->width, in);
        fseek(in, line_offset, SEEK_CUR);
        uint32_t image_pixels_offset = (pix_buf->height - (i + 1)) * pix_buf->width;
        for (int j = 0; j < pix_buf->width; ++j) {
            pix_buf->data[image_pixels_offset + j] = pixel_line[j];
        }
    }
    free(pixel_line);
}
