#include "bmp.h"

static unsigned char sat(uint64_t x);

struct bmp_info * new_bmp() {
    struct bmp_info * bmp =  malloc(sizeof(struct bmp_info));
    bmp->img = malloc(sizeof(struct image));
    return bmp;
}

struct image * rotate_image(const struct image * source) {
    struct image * result = malloc(sizeof(struct image));
    result->width = source->height;
    result->height = source->width;
    result->data = malloc(sizeof(struct pixel) * result->width * result->height);
    for (int i = 0; i < source->width; i++) {
        for (int j = 0; j < source->height; j++) {
            result->data[(i + 1) * source->height - (j + 1)] = source->data[j * source->width + i];
        }
    }
    return result;
}

void rotate(struct bmp_info * source_bmp){
    uint32_t width = source_bmp->bmi->bi_width;
    source_bmp->bmi->bi_width = source_bmp->bmi->bi_height;
    source_bmp->bmi->bi_height = width;
    source_bmp->img = rotate_image(source_bmp->img);
}

struct image * sepia(struct image * source) {
    struct image * result = malloc(sizeof(struct image));
    result->width = source->width;
    result->height = source->height;
    result->data = malloc(sizeof(struct pixel) * result->width * result->height);
    for (int i = 0; i < source->width * source->height; ++i) {
        result->data[i] = * sepia_one(&source->data[i]);
    }
    return result;
}

struct pixel * sepia_one(struct pixel * const pixel) {
    static const float c[3][3] = {
            {.393f, .769f, .189f},
            {.349f, .686f, .168f},
            {.272f, .534f, .131f}};
    struct pixel const old = * pixel;
    pixel->r = sat(
            old.r * c[0][0] + old.g * c[0][1] + old.b * c[0][2]
    );
    pixel->g = sat(
            old.r * c[1][0] + old.g * c[1][1] + old.b * c[1][2]
    );
    pixel->b = sat(
            old.r * c[2][0] + old.g * c[2][1] + old.b * c[2][2]
    );
    return pixel;
}

static unsigned char sat(uint64_t x) {
    return x < 256 ? x : 255;
}
