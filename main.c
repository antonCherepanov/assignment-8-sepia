#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "bmp.h"
#include "write_bmp.h"
#include "read_bmp.h"
#include "sepia_sse.h"

#define BUFFER_SIZE 15

static bool check_exit(char* input);

int main() {
    char filename [BUFFER_SIZE];
    puts("Enter the name of the file to covert");
    fgets(filename, BUFFER_SIZE, stdin);
    if(check_exit(filename)) return 0;
    struct bmp_info * bmp = new_bmp();
    strtok(filename, "\n");
    char new_filename [BUFFER_SIZE];
    FILE * source = fopen(filename, "r");
    if(source == NULL){
        printf("Unable to open file with the name %s\n", filename);
        return 0;
    }
    from_bmp(source, bmp);
    puts("Choose an option.\nEnter \"c\" or \"asm\"");
    char option [BUFFER_SIZE];
    fgets(option, BUFFER_SIZE, stdin);
    bool hasChoosenAssembly = !strcmp(option, "asm\n");
    bool hasChoosenC = !strcmp(option, "c\n");
    if(check_exit(filename)) return 0;
    if(hasChoosenC || hasChoosenAssembly){
        sprintf(new_filename, "sepia.bmp");
        FILE * dest = fopen(new_filename, "w");
        if(hasChoosenAssembly){
            struct image * res = malloc(sizeof(struct image));
            sepia_sse(bmp->img, res);
            bmp->img = res;
        }else{
            bmp->img = sepia(bmp->img);
        }
        to_bmp(dest, bmp);
        fclose(dest);
    }else{
        puts("Illegal option was provided");
    }
    fclose(source);
    return 0;

    // //for tests
    // struct bmp_info *bmp = new_bmp();
    // char *filename = "large.bmp";
    // char new_filename[15];
    // FILE *source = fopen(filename, "r");

    // sprintf(new_filename, "sepia.bmp");
    // FILE *dest = fopen(new_filename, "w");
    // from_bmp(source, bmp);
    // //c
    // bmp->img = sepia(bmp->img);
    // //asm
    // // struct image *res = malloc(sizeof(struct image));
    // // sepia_sse(bmp->img, res);
    // // bmp->img = res;

    // to_bmp(dest, bmp);
    // return 0;
}

static bool check_exit(char * input){
    return !(strcmp(input, "quit\n") && strcmp(input, "q\n") && strcmp(input, "quit") && strcmp(input, "q"));
}